---
title: "WRDS"
date: 2020
icon: "ti-server"
description: The Wharton Research Data Services (WRDS) is a web based database service developped by the Wharton School, University of Pennsylvania.
keywords: wrds, Wharton Research Data Services, sas, compustat, crsp
type : "docs"
---

