---
title: Finding a table
keywords: wrds libraries, table names
description: "WRDS: finding a table through SAS Studio"
weight: 0
---

## Table name

WRDS offers access to a collection of datasets which can also be accessed through SAS/Studio.

{{%notice info%}}
Access to specific datasets depends on your institution's licence.
{{%/notice%}}

1. Click on the `Libraries` menu on the left panel and double click the `FRB > FX_MONTHLY` table.

![](click_lib.png)

2. You can access more details about the dataset by clicking on the `Table Properties` button.

![](sas_options.png)

3. To create `Filter` expressions you may use `SQL` syntax as you would within a `SAS SQL` <a href="https://support.sas.com/documentation/cdl//en/sqlproc/69822/HTML/default/viewer.htm#titlepage.htm" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> procedure's `WHERE` clause.

![](sas_filter.png)

4. SAS/Studio lets you export the `SAS` syntax that created the current table.

![](sas_export_code.png)

5. In this example, the output is a `.sas` file located at `/home/ulaval/yourusername/example/mon_program.sas` which you can run to recreate the table.

![](sas_save_code.png)

## `.sas` file

1. Open the `mon_program.sas` file and click on the `Run all or selected code (F3)` button.

![](sas_run_code.png)

2. You will find the program log by clicking on the `LOG` tab. The results as well as the output tables are available via the `RESULTS` and `OUTPUT DATA` tabs respectively.

![](sas_code_output.png)

## Download
If for some reason you need to keep a local copy of a dataset, you can right click on the file to `Export` to any of the following available formats. Note however that some datasets are quite large (>10GB) and it is recommended that you avoid storing local copies of such tables.

![](sas_export.png)

{{%notice tip%}}
It's also possible to download the results of a `SAS` program as an `HTML`, `PDF` or `WORD` file through the `RESULTS` tab.
{{%/notice%}}
