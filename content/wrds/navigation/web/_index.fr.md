---
title: Requête web
keywords: ratios financiers, wrds, wrds ratios, financial ratios suite, web wrds, wrds  web, requête web, requête wrds
description: "WRDS : exemples de requête avec l'interface web."
weight: 0
---

Les exemples ci-bas vous montre comment faire une requête sur WRDS avec l'interface web.

> Les instructions pour créer un compte WRDS se trouvent [ici](../../connection/username).

## Ratios financiers

{{%tabs%}}
{{%tab "Instructions"%}}

### Ratios financiers

1. Cliquez sur `Financial Ratios Suite > Financial Ratios Industry Level`.
2. Complétez les champs de la façon suivante :
  + **Step 1** : `Date Range` de `1970-01` à  `aujourd'hui`.
  + **Step 2** : `Universe` : `CRSP US Common Stocks`.
  + **Step 3** : `Industry Classification` : `GICS 10 sectors`.
  + **Step 4** : `Variables` : `All`.
  + **Step 5** : `Industry Aggregation` : `Median`.
  + **Step 6** : `Output Format` : `comma-delimited text (*.csv)`.
3. Faites un clic droit sur le fichier `.csv` résultant et cliquez sur `Enregistrer le lien sous ...`.
4. Choisissez un dossier sur votre ordinateur, par exemple `Téléchargements`, et cliquez sur `Enregistrer`.


{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Financial Ratios Suite](wrds-ratios.gif)
{{%/tab%}}

{{%/tabs%}}

## CRSP

{{%tabs%}}
{{%tab "Instructions"%}}

### Prix historiques

1. Cliquez sur `CRSP > Index / S&P 500 Indexes > CRSP Index File on the S&P 500`.
2. Complétez les champs de la façon suivante :
  + **Step 1** : `Date Range` de `1925-12` à  `aujourd'hui` ou le maximum indiqué. `Data Frequency : Monthly`.
  + **Step 2** : `Variables` : `Return on S&P Composite Index, Level on S&P 500 Composite Index`.
  + **Step 3** : `Output Format` : `comma-delimited text (*.csv)`.
3. Faites un clic droit sur le fichier `.csv` résultant et cliquez sur `Enregistrer le lien sous ...`.
4. Choisissez un dossier sur votre ordinateur, par exemple `Téléchargements`, et cliquez sur `Enregistrer`.


{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS CRSP](wrds-crsp.gif)
{{%/tab%}}

{{%/tabs%}}
