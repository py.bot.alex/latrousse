---
title: Python
keywords: wrds, python, python query, python wrds connection, python connection,wrds connection
description: "WRDS: how to connect using Python."
weight: 0
---

{{%notice note%}}
You can download `python` by visiting the official website at `https://www.python.org`. However, it's recommended to install the [Anaconda distribution](https://www.anaconda.com/products/individual) instead which includes `python` and other popular tools for data analysis such as the `Jupyter Notebook`.
{{%/notice%}}

# Connection

1. Install the `wrds` module using `pip`. You can use either the `Jupyter Notebook` {{%extlink "https://docs.anaconda.com/ae-notebooks/user-guide/basic-tasks/apps/jupyter/"%}}, your prefered [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) or the `WRDS Cloud` {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/"%}} to follow these instructions.

{{% tabs %}}

{{% tab "Terminal" %}} ![](install_wrds.gif) {{% /tab %}}
{{% tab "Jupyter Notebook" %}} ![](install_wrds_jupyter.gif) {{% /tab %}}

{{% /tabs %}}

2. Connect to `wrds` using the `wrds.Connection()` function. It is also recommended that you create a file named `.pgpass` in your `home` folder either manually or using the `db.create_pgpass_file()` function from the `wrds` module you just installed. The file must have the following line where `wrds_username` and `wrds_password` are your WRDS user name and password.

```
wrds-pgdata.wharton.upenn.edu:9737:wrds:wrds_username:wrds_password
```

{{%notice info%}}
Your `home` or `~/` folder's locations depends on your operating system. On Unix-like systems it's located at `/Users/yourusername` (MAC) or `/home/yourusername`, on Windows it's called the `%APPDATA%` folder and it's located at something similar to `C:\Users\yourusername\AppData\Roaming`.
{{%/notice%}}

{{% tabs %}}

{{% tab "Terminal" %}}
![](pgpass.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](pgpass_jupyter.gif)
{{%notice note%}}
If you create the `.pgpass` file manually on Windows, please avoid using the well known Notepad because you need to pay extra attention to its default extension settings. Instead, you could try [Notepad++](https://notepad-plus-plus.org/downloads/)(open-source license) our [Sublime Text](https://www.sublimetext.com/) (proprietary license).
{{%/notice%}}
{{% /tab %}}

{{% /tabs %}}

{{% notice info %}}
If you create the `.pgpass` file manually, make sure you assign the file permissions as explained in the `PostgreSQL` documentation {{%extlink "https://www.postgresql.org/docs/9.3/libpq-pgpass.html"%}} . Ignore this if working on Windows.
{{%/notice%}}


Test your connection with the following `SQL` query :

```python
df = db.raw_sql('SELECT date,dji FROM djones.djdaily LIMIT 5;')  # LIMIT the number of records
```

You should find the first `5` lines of the `djones.daily` dataset in the `df` variable stored as a `pandas.DataFrame`.

{{% tabs %}}
  {{% tab "Terminal" %}}
  ![](test_wrds.gif)
  {{% /tab %}}

  {{% tab "Jupyter Notebook" %}}
  ![](test_wrds_jupyter.gif)
  {{% /tab %}}

{{% /tabs %}}

{{%notice tip%}}
Additional query examples are available on the WRDS website {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/querying-wrds-data-python"%}} .
{{%/notice%}}
