---
title: SAS SQL
keywords: sas sql, csv, export sas data, date format, sas date, sas wrds, wrds sas, sas studio
description: "WRDS : SQL query using SAS Studio."
weight: 0
---

We will use `SQL` syntax to query the `WRDS` database and extract a `subset` of the `FRB.FX_MONTHLY` table.

{{%notice note%}}
You can find more details about the `SAS SQL` prodecedure on the official SAS documentation website <a href="https://support.sas.com/documentation/cdl//en/sqlproc/69822/HTML/default/viewer.htm#p07v6ho0hymhfvn1jboqfe38jnox.htm" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> .
{{%/notice%}}

# Monthly currency data

This sample code creates a table named `myforex` by selecting the `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` and `exukus` columns from the `FRB.FX_MONTHLY` table which is preloaded in the workspace by `WRDS`, so we can reference it using the `FRB.FX_MONTHLY` syntax.

```
PROC SQL;
     CREATE TABLE myforex AS
     SELECT 'date'n , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus
     FROM FRB.FX_MONTHLY
     WHERE date > '01JAN2000'd;
QUIT;

PROC PRINT DATA=myforex (OBS=5);
RUN;
PROC MEANS DATA=myforex;
RUN;
```

The `WHERE` clause filters the dataset to return the lines where the `date` column is greater than `01JAN2000`. Note that the `d` after the `'` is important, otherwise the date `characters` will not be treated as a `d`ate and the comparison will brake with an error complaining about `different data types`. Unless you received an error, this is what you will find in the `RESULTS` tab :

![](comp_forex.png)

# Changing de date format

Just like `Excel`, `SAS` stores dates as `numeric` values. We can control the way a `date` value is displayed by using the appropriate `format` <a href="https://documentation.sas.com/?docsetId=ds2pg&docsetTarget=p0bz5detpfj01qn1kz2in7xymkdl.htm&docsetVersion=3.1&locale=en" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> .

```
DATA myforex2;
  SET myforex;
  FORMAT date DDMMYY10.;
RUN;

PROC PRINT DATA=myforex2 (OBS=5);
RUN;
```

Executing the above code will change the date format to `DDMMYY10.` as shown in the results below.
![](date_format.png)

# Exporting to CSV
If you wish to keep a copy of a data table, you can use the `PROC EXPORT` <a href="https://documentation.sas.com/?docsetId=proc&docsetTarget=n0qarv79909fdhn13c60hfubqwdq.htm&docsetVersion=9.4&locale=en" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> procedure as in the following example which will save a copy of the `myforex2` dataset as `forex_prices.csv` in the `~/example` folder :


{{%notice note%}}
The shortcut notation `~/` references your `home` folder, located at `/home/ulaval/yourusername/`. This syntax is used in unix-like systems <a href="https://en.wikipedia.org/wiki/Unix-like" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> such as the WRDS service <a href="https://wrds-www.wharton.upenn.edu/pages/support/support-articles/general-information/accessing-wrds/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.
{{%/notice%}}
```
PROC EXPORT DATA=myforex2
     OUTFILE="~/example/forex_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

# Full example
The following `SAS` scripts will create three `CSV` files out of three `WRDS` datasets using `SAS/Studio`. The resulting datasets contain currency rates from the `FRB.FX_MONTHLY` table, FTSE Indices prices from the `COMPD.G_IDX_MTH` table and FTSE Indices names by `gvkeyx` from the `COMP.G_NAMES_IX` table. Finally, we can simply right click on the `file` we'd like to download :

![](csv_files.png)

The resulting `CSV` files can then be opened using any software.
![](csv_view.png)

## SAS Code

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Create the `myforex` table :

```sas
PROC SQL;
     CREATE TABLE myforex AS
     SELECT 'date'n , excaus , exchus , exhkus ,
            exinus , exjpus , exkous ,exmxus , exukus
     FROM FRB.FX_MONTHLY
     WHERE date > '01JAN2000'd;
QUIT;

```

2. Modify the `date format` if needed :

```sas
/* Date format */
DATA myforex2;
  SET myforex;
  FORMAT date DDMMYY10.;
RUN;
```

3. Export the `myforex2` table as `forex_prices.csv` :

```sas
/* Export to CSV */
PROC EXPORT DATA=myforex2
	 OUTFILE="~/example/forex_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% tab "index_names.csv" %}}


1. Create the `gvnames` table :

```sas
PROC SQL;
     CREATE TABLE gvnames AS
     SELECT *
     FROM COMP.G_NAMES_IX
     WHERE gvkeyx IN ('150004', '150979', '150984', '150994');
QUIT;
```

2. Export the `gvnames` table as `index_names.csv` :

```sas
PROC EXPORT DATA=gvnames
	 OUTFILE="~/example/index_names.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% tab "index_prices.csv" %}}


1. Create the `myindex` table :

```sas
PROC SQL;
     CREATE TABLE myindex AS
     SELECT gvkeyx, datadate, prccm, prchm, prclm
     FROM COMPD.G_IDX_MTH
     WHERE gvkeyx IN ('150004', '150979', '150984', '150994') AND
     datadate > '01JAN2000'd;
QUIT;
```

2. Export the `myindex` table as `index_prices.csv` :

```sas
PROC EXPORT DATA=myindex
	 OUTFILE="~/example/index_prices.csv"
     DBMS=csv
     REPLACE;
RUN;
```

{{% /tab %}}

{{% /tabs %}}


