---
title: Python
keywords: python, sql, csv, exporter, données, format, date, dates, requête sql, python sql
description: "WRDS : Requête SQL via Python."
weight: 0
---

Nous allons utiliser la syntaxe `SQL` pour appliquer des filtres et extraire certaines données du tableau `FRB.FX_MONTHLY` disponible sur `WRDS`.

{{%notice note%}}
Vous trouverez plus de détails sur le module `wrds` dans la documentation officielle {{%extlink "https://github.com/wharton/wrds"%}}. Un guide complet est disponible sur le site de WRDS {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/querying-wrds-data-python/"%}}.
{{%/notice%}}

# Données sur les taux de change
L'exemple de requête ci-bas permet de créer le tableau `myforex` en choisissant les colonnes `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` et `exukus` du tableau `FRB.FX_MONTHLY` lequel est disponible grâce à la connexion `wrds` créée avec la fonction `wrds.Connection()`. Cette configuration nous permet d'accéder au tableau avec la syntaxe `FRB.FX_MONHTLY`.

```python
import wrds
db = wrds.Connection(wrds_username = "wrds_username")
myquery = "SELECT date , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus FROM FRB.FX_MONTHLY WHERE date > '2000-01-01'"
myforex = db.raw_sql(myquery, date_cols=['date'])

myforex.head()
myforex.describe()
```

La ligne `WHERE` permet de filtrer pour n'obtenir que les données dont la valeur de la colonne `date` est supérieure à `2000-01-01` (**yyyy-mm-dd**).

{{% tabs %}}

{{% tab "Terminal" %}}
![](query_forex.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](query_forex_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

# Changer le format des dates

Comme c'est le cas avec `Excel`, `SAS` et `R`, `Python` sauvegarde les dates {{%extlink "https://docs.python.org/3.7/library/datetime.html"%}} comme des valeurs de type `numérique`. Pour contrôler comment la valeur de la colonne `date` est affichée, il suffit d'utiliser la méthode `strftime` pour modifier le `format` en conséquence <a href="https://docs.python.org/3.7/library/datetime.html#strftime-and-strptime-behavior" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>. L'output sera un object  de type `string` {{%extlink "https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str" %}}.

```python
myforex['datestring'] = myforex['date'].dt.strftime('%d/%m/%Y')
```

L'exécution du code ci-haut change le `format` de la colonne `date` à `%d/%m/%Y` et sauvegarde le résultat dans une nouvelle colonne `datestring`.

{{% tabs %}}

{{% tab "Terminal" %}}
![](date_format.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](date_format_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

# Exporter à CSV

Il est possible de créer une copie des données avec la fonction `write.csv` <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>. L'exemple ci-bas sauvegarde une copie du tableau `myforex` comme `forex_prices.csv` dans le dossier `~/example`.

```python
myforex.to_csv("forex_prices.csv")
```

{{% tabs %}}

{{% tab "Terminal" %}}
![](csv_export.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](csv_export_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

# Exemple complet
Les commandes `Python` ci-bas créeront trois fichiers `.csv` à partir de trois tableaux `wrds` à l'aide du module `wrds`. Les fichiers résultats contiennent les taux de change obtenus à partir du tableau `FRB.FX_MONHTLY`, les prix des indices FTSE du tableau `COMP.G_IDX_MTH` et leurs noms qui se trouvent sur le tableau `COMP.G_NAMES_IX`.

{{% tabs %}}

{{% tab "Terminal" %}}
![](full.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](full_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

Les fichiers `.csv` résultants peuvent être ouverts à partir de tout logiciel.
![](csv_view.png)

## Code Python

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Créez le tableau `myforex` :

```python
import wrds
db = wrds.Connection()

myforex = db.raw_sql("SELECT date , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus FROM FRB.FX_MONTHLY WHERE date > '2000-01-01'",
               date_cols=['date'])
```

2. Modifiez le `format` de la colonne `date` au besoin :

```python
myforex['datestring'] = myforex['date'].dt.strftime('%d/%m/%Y')
```

3. Sauvegardez le tableau `myforex` comme `forex_prices.csv` :

```python
myforex.to_csv("forex_prices.csv", index = False)
```

{{% /tab %}}

{{% tab "index_names.csv" %}}

1. Créez le tableau `gvnames` :

```python
gvnames = db.raw_sql("SELECT * FROM COMP.G_NAMES_IX WHERE gvkeyx IN ('150004', '150979', '150984', '150994')")
```

2. Sauvegardez le tableau `gvnames` comme `index_names.csv` :

```python
gvnames.to_csv("index_names.csv", index = False)
```
{{% /tab %}}

{{% tab "index_prices.csv" %}}


1. Créez le tableau `myindex` :

```python
myindex = db.raw_sql("SELECT gvkeyx, datadate, prccm, prchm, prclm FROM COMP.G_IDX_MTH WHERE gvkeyx IN ('150004', '150979', '150984', '150994') AND datadate > '2000-01-01'",
               date_cols=['datadate'])
```

2. Sauvegardez le tableau `myindex` comme `index_prices.csv` :

```python
myindex.to_csv("index_prices.csv", index = False)
```

{{% /tab %}}

{{% /tabs %}}


