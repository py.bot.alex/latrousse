---
title: Python
keywords: python, sql, csv, export, data, date, format, wrds python, python sql
description: "WRDS : SQL query using Python."
weight: 0
---

We will use `SQL` syntax to query the `WRDS` database and extract a `subset` of the `FRB.FX_MONTHLY` table using the `wrds` module .

{{%notice note%}}
You can find more details about the `wrds` module on the official website {{%extlink "https://github.com/wharton/wrds"%}}. A complete guide is available at the WRDS website {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/querying-wrds-data-python/"%}}.
{{%/notice%}}

# Monthly currency data

This sample code creates a `pandas.DataFrame` named `myforex` by selecting the `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` and `exukus` columns from the `FRB.FX_MONTHLY` table which is accessible via the `wrds` connection created using the `wrds.Connection()` function. This setup allows us to access the table using the `FRB.FX_MONTHLY` syntax.

```python
import wrds
db = wrds.Connection(wrds_username = "wrds_username")
myquery = "SELECT date , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus FROM FRB.FX_MONTHLY WHERE date > '2000-01-01'"
myforex = db.raw_sql(myquery, date_cols=['date'])

myforex.head()
myforex.describe()
```

The `WHERE` clause filters the dataset to return the lines where the `date` column is greater than `2000-01-01` (**yyyy-mm-dd**).

{{% tabs %}}

{{% tab "Terminal" %}}
![](query_forex.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](query_forex_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

# Changing de date format

Just like `Excel`, `SAS` and `R`, `Python` stores dates {{%extlink "https://docs.python.org/3.7/library/datetime.html"%}} as `numeric` values internally. We can control the way a `datetime` value is displayed by using the `strftime` method <a href="https://docs.python.org/3.7/library/datetime.html#strftime-and-strptime-behavior" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> which outputs a `string` object {{%extlink "https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str" %}}.

```python
myforex['datestring'] = myforex['date'].dt.strftime('%d/%m/%Y')
```

Executing the above code will change the date `format` to `%d/%m/%Y` and save it as a `string` object named `datestring` as shown in the results below.

{{% tabs %}}

{{% tab "Terminal" %}}
![](date_format.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](date_format_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

# Exporting to csv
If you wish to keep a copy of a data table, you can use the `pandas.DataFrame.to_csv` method <a href="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html" target="_blank"><i class="fas fa-external-link-square-alt"></i></a> as in the following example which will save a copy of the `myforex` dataset as `forex_prices.csv` in the `~/example` folder :

```python
myforex.to_csv("forex_prices.csv")
```

{{% tabs %}}

{{% tab "Terminal" %}}
![](csv_export.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](csv_export_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}


# Full example
The following `Python` commands will create three `.csv` files out of three `wrds` datasets using the `wrds` module. The resulting datasets contain currency rates from the `FRB.FX_MONTHLY` table, FTSE Indices prices from the `COMP.G_IDX_MTH` table and FTSE Indices names by `gvkeyx` from the `COMP.G_NAMES_IX` table.


{{% tabs %}}

{{% tab "Terminal" %}}
![](full.gif)
{{% /tab %}}

{{% tab "Jupyter Notebook" %}}
![](full_jupyter.gif)
{{% /tab %}}

{{% /tabs %}}

The resulting `CSV` files can then be opened using any software.

![](csv_view.png)

## Python Code

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Create the `myforex` table :

```python
import wrds
db = wrds.Connection()

myforex = db.raw_sql("SELECT date , excaus , exchus , exhkus , exinus , exjpus , exkous ,exmxus , exukus FROM FRB.FX_MONTHLY WHERE date > '2000-01-01'",
               date_cols=['date'])
```

2. Modify the `date format` if needed :

```python
myforex['datestring'] = myforex['date'].dt.strftime('%d/%m/%Y')
```

3. Export the `myforex` table as `forex_prices.csv` :

```python
myforex.to_csv("forex_prices.csv", index = False)
```

{{% /tab %}}

{{% tab "index_names.csv" %}}


1. Create the `gvnames` table :


```python
gvnames = db.raw_sql("SELECT * FROM COMP.G_NAMES_IX WHERE gvkeyx IN ('150004', '150979', '150984', '150994')")
```

2. Export the `gvnames` table as `index_names.csv` :

```python
gvnames.to_csv("index_names.csv", index = False)
```
{{% /tab %}}

{{% tab "index_prices.csv" %}}

1. Create the `myindex` table :

```python
myindex = db.raw_sql("SELECT gvkeyx, datadate, prccm, prchm, prclm FROM COMP.G_IDX_MTH WHERE gvkeyx IN ('150004', '150979', '150984', '150994') AND datadate > '2000-01-01'",
               date_cols=['datadate'])
```

2. Export the `myindex` table as `index_prices.csv` :

```python
myindex.to_csv("index_prices.csv", index = False)
```

{{% /tab %}}

{{% /tabs %}}


