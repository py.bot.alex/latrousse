---
title: "Refinitiv Workspace"
date: 2020
icon: "ti-signal"
description: Le logiciel Refinitiv Workspace (ancien Eikon) offre une interface intuitive pour l'analyse de données financières historiques ou en temps-réel.
keywords: eikon, logiciel, terminal, refinitiv, workspace
type : "docs"
---

