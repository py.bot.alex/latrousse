---
title: Notation
keywords: syntax, r, notation, vector, matrix, learn r
description: "The basics of R : vectors and matrices"
weight: 0
---

## Vectors


{{%tabs%}}

{{%tab "Code"%}}

1. Create a vector and print its content.

```r
c(2,3,5,7)  # Shown but not saved
mon_vecteur <- c(2,3,5,7)  # Use <- instead of =
print(mon_vecteur)
```

2. Use the `length()` function to know the length of the vector:

```r
mon_vecteur <- c(2,3,5,7)  # Saves to mon_vecteur
length(mon_vecteur)
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](vector.gif)
{{%/tab%}}


{{%/tabs%}}


## Matrix


{{%tabs%}}

{{%tab "Code"%}}

1. To create a 4 by 2 matrix:

```r
matrix(data=c(2,3,5,7,2,4,3,2), nrow=4, ncol=2)
```

To save the result, you must use the `<-` symbol.

2. To raise each element of the matrix to the 2 power, we use the same syntax as in `Excel` :

```r
ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)
ma_matrice^2  # Squared
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](matrix.gif)
{{%/tab%}}


{{%/tabs%}}

## Indexing


```r
mon_vecteur <- c(2,3,5,7)
mon_vecteur[3]  # 3rd element of vector

ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)
ma_matrice[3,2]  # Prints 3, namely the element in row 3 column 2
```

> We can also access multiple values as in `ma_matrice[c(1, 3), c(1, 2)]` which returns the first and third line of the matrix (lines = `c(1,3)`), columns 1 and 2 (columns = `c(1, 2)`).


