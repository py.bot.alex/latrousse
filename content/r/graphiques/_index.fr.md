---
title: Graphiques
keywords: graphiques, r, charts, ggplot2, plot, histogramme
description: Création de graphiques avec R et ggplot2.
weight: 0
---

## Plot et ggplot2

Pour créer des graphiques sur `R`, on utilise principalement la fonction `plot()`. Par exemple, nous pouvons créer un graphique à partir de la matrice `ma_matrice` avec la commande `plot(ma_matrice)`. La fonction `plot()` prend plusieurs autres arguments si l'on désire personnaliser notre graphique. Par exemple, l'argument `main` permet d'ajouter un titre, tandis qu'il est possible d'ajouter des libellés pour l'axe des $x$ avec l'argument `xlab`.

Il est aussi possible de créer toute sorte de graphiques sur `R` à l'aide de `paquetages`. Par exemple, consultez la fonction `qplot` du paquetage `ggplot2` <a href="https://ggplot2.tidyverse.org/" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>:

{{%tabs%}}

{{%tab "Code"%}}

### Base

```r
ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)  # Les données
plot(ma_matrice^2)                              # Le graphique du carré des données
# plot(ma_matrice^2, main="Mon Titre", xlab="Axe des X", ylab="Axe des Y")  # Effacez le premier "#" pour exécuter
```

### ggplot2

```r
library(ggplot2)
ma_matrice_2 <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)^2  # Les données au carré
mes_x <- ma_matrice_2[, 1]  # Colonne 1
mes_y <- ma_matrice_2[, 2]  # Colonne 2

ggplot2::qplot(mes_x, mes_y, main = "Mon titre", xlab="Axe des X", ylab="Axe des Y")
```

{{%/tab%}}

{{%tab "base"%}}
![](plot_output.png)
{{%/tab%}}

{{%tab "ggplot2"%}}
![](qplot_output.png)
{{%/tab%}}

{{%/tabs%}}

## Histogramme

Nous commençons par obtenir les prix historiques de `XGD.TO` en suivant les instructions qui se trouvent [ici](../csv/#).

{{%tabs%}}

{{%tab "Code"%}}

### Base

1. Créez l'histogramme des prix de la colonne `Open` du tableau `donnees` avec la commande `hist`:

```r
hist(donnees$Open, main="Mon Titre", xlab="Open Price")
```


### ggplot2


1. Créez l'histogramme des prix de la colonne `Open` du tableau `donnees` avec la commande `qplot`:

```r
library(ggplot2)
ggplot2::qplot(donnees$Open, geom="histogram",
               main = "Mon titre", xlab="Open Price", ylab="Frequency")
```

{{%notice tip%}}
Sauvegardez le graphique en format `png` ou `pdf` avec les commandes suivantes.
{{%/notice%}}

```r
png("histogramme.png")  # Début du fichier

# Le graphique
ggplot2::qplot(donnees$Open, geom="histogram",
               main = "Mon titre",
               xlab="Axe des X",
               ylab="Axe des Y")

dev.off()  # Fin, important !
```


> Utilisez `pdf("histogramme.pdf")` au lieu de `png("histogramme.png")` pour créer un fichier `pdf`.


{{%/tab%}}

{{%tab "base"%}}
![](hist_base.png)
{{%/tab%}}

{{%tab "ggplot2"%}}
![](hist_ggplot2.gif)
{{%/tab%}}

{{%/tabs%}}



