---
title: CSV Files
keywords: how to read a csv file, csv r, csv file, read data on r
description: How to read a CSV file using R
weight: 0
---

## Importing data

To read a `.csv` file on `R` we use the `read.csv` function. This function takes as first argument the name of the file, e.g. `monfichier.csv`.

```r
donnees <- read.csv("monfichier.csv", sep=",")
```

{{%tabs%}}

{{%tab "Instructions"%}}

1. Get some historical prices for `XGD.TO` through `https://ca.finance.yahoo.com/`.

2. Read the `CSV`  data into `R` using `read.csv()` and save it in a variable named `donnees` :

```r
donnees <- read.csv("Downloads/XGD.TO.csv")
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](csv.gif)
{{%/tab%}}

{{%/tabs%}}

