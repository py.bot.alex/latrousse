---
title: Langue
keywords: changer la langue, excel en anglais, word en anglais, office en anglais, langue excel
description: Comment changer la langue d'affichage d'Excel.
weight: -1
---

# Langue d'affichage

La langue d'affichage d'Excel dépend des configurations de votre ordinateur (version locale) et de votre navigateur web (version web).

{{%notice info%}}
Les formules Excel fonctionnent sans problèmes majeurs et ce peu importe la langue d'affichage. Portez attention aux délimiteurs `;` et `,`.
{{%/notice%}}

## Version locale

Si vous travaillez sur Excel localement (sur votre ordinateur), voici comment choisir la langue d'affichage :

1. Lancer `Excel`sur l'ordinateur.
2. Cliquer sur `Fichier > Options > Langue`.
3. Choisir la langue d'affichage en cliquant sur `Monter`. Télécharger d'autres langues au besoin.

![`Options > Langue`](options_langues.png)


4. Cliquer sur `OK` et redémarrer Excel pour que les modifications se fassent.

![`Options OK`](options_ok.png)

{{% notice info %}}
Sur Windows, ceci change la langue des produits de la suite Office. Sur MAC, il faut modifier la langue du système exploitation en suivant [ces](https://support.office.com/fr-fr/article/changer-la-langue-utilis%c3%a9e-par-office-dans-ses-menus-et-outils-de-v%c3%a9rification-linguistique-f5c54ff9-a6fa-4348-a43c-760e7ef148f8?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA#ID0EAACAAA=MacOS) instructions.
{{% /notice %}}

## Version web

La langue d'affichage sur la version web dépend des paramètres de votre navigateur (Chrome, Brave, Firefox, etc.).


![`Accueil EN`](home_eng.png)


1. Aller aux paramètres du navigateur.
2. Trouver les paramètres de langue. La langue d'affichage sera celle en haut de la liste.

![`Settings`](lang_eng.png)


3. Cliquer sur `Monter/Descendre` au besoin.

![`UpDown`](lang_ok.png)


4. Retourner sur la page `portal.office.com`
5. Cliquer sur le bouton affiché ci-bas et ensuite sur `Paramètres du site`.

![`Site settings`](site_settings.png)


7. Cliquer sur `Effacer les données du site`.

![`Clean settings`](site_clean.png)


8. Retourner sur `portal.office.com` et ouvrir Excel.

![`Acuueil FR`](home_fr.png)


