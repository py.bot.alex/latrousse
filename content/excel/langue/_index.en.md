---
title: Language
keywords: change language, excel language, word language, office in English, language
description: How to set the display language on Excel.
weight: -1
---

# Display language

The language of your Office menus depend on your computer's settings, the web version will use the default browser language.

{{%notice info%}}
Excel functions will work without major issues regardless of the display language. Be careful when using `;` or `,` as delimiters.
{{%/notice%}}

## Local version

Here's how to change the display language of your locally installed Excel :

1. Launch `Excel` locally (on your computer).
2. Click on `File > Options > Language`.
3. Bring your preferred language to the top of the list by clicking on the `Up` button. You may also download additional languages if needed.

![`Options > Language`](options_langues.png)

4. Click on `OK` and restart Excel.

![`Options OK`](options_ok.png)

{{% notice info %}}
This will change the display language of all Office products on Windows. On MAC, you will need to change the operating system's language (instructions [here](https://support.office.com/en-us/article/change-the-language-office-uses-in-its-menus-and-proofing-tools-f5c54ff9-a6fa-4348-a43c-760e7ef148f8#ID0EAACAAA=MacOS)).
{{% /notice %}}

## Web version

The display language of the web version depends on your web browser's (Firefox, Chrome, Brave, etc.) settings.

![`Home EN`](home_eng.png)

1. Go to your broswer's settings.
2. Find the language settings section. The language on the top of the list will be used as default.

![`Settings`](lang_eng.png)

3. Click on  `Move up/Move down` as needed.

![`UpDown`](lang_ok.png)

4. Go back to the Office 365 home page at `portal.office.com`
5. Click on the button shown below and then on `Site settings`.

![`Site settings`](site_settings.png)

7. Click on `Clear site data`

![`Clean settings`](site_clean.png)

8. Go back to `portal.office.com` and launch Excel.

![`Home FR`](home_fr.png)


