---
title: Notation
keywords: array formulas, matrix formulas, excel names
description: A quick tour of some Excel basics.
weight: -3
---

When working with Excel, the cell reference `A1` corresponds to column `A`, row `1` of a spreadsheet. For instance, if we find the number $2$ on cell `A1`, we denote this using the syntax `A1 = 2`.

## Vectors

Let's say we'd like to create the vector `$(2,3,5,7)$` on Excel. We can see this vector as either a row or column on our spreadsheet. In the example below, we find the first element of our vector on cell `A1`, the second element on cell `A2`, etc. By default, Excel denote this vector using the `A1:A4` syntax, where `A1 = starting point` and `A4 = end point`.

![`A1:A4 = (2,3,5,7)`](excel-c.png)

<i class="fa fa-table"></i> If we want to find out the length of our newly created vector, we ask Excel to return this length by calling the `COUNT()` function. In our example, the Excel formula `A5 = COUNT(A1:A4)` will return the number `4`, namely the number of elements in our vector.

![`A5 = COUNT(A1:A4)`](excel-nb.png)

More often than not, we use customs names instead of the default `A1:A4` notation. For instance, we might rather call our vector `MonVector` by first selecting the excel range and then clicking on the upper left box. Next, all we have to do is enter any name respecting Excel's syntax rules <a href="https://support.office.com/fr-fr/article/d%c3%a9finir-et-utiliser-des-noms-dans-les-formules-4d0f13ac-53b7-422e-afd2-abd7ff379c64?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-alt"></i></a> and hit `<Enter>`.

![`MonVector= A1:A4`](excel-names.png)

<i class="fa fa-table"></i> Now, instead of using the default `A1:A4` syntax when we call the `COUNT()` function, we may simply say `COUNT(MonVector)` which is hopefully easier to remember.

![`A5 = COUNT(MonVector)`](excel-names-2.png)

## Matrices

A `$M_{i\times j}$` matrix is an excel table with `$i$` rows and `$j$` columns. We create the following matrix using `4` rows and `2` columns:

$$
M_{4\times2}=
  \begin{bmatrix}
    2 & 2 \\\\
    3 & 4 \\\\
    5 & 3 \\\\
    7 & 2
  \end{bmatrix}
$$

We have the choice between using the default `A1:B4` syntax or using a custom name as  we did with vectors. In this example we are using `MaMatrice` as name.

![`MaMatrice = A1:B4`](excel-matrix.png)

<i class="fa fa-table"></i> Here's an example of using an array formula to calculate the squared of each element of our matrix :

![`{D1:E4=MaMatrice^2}`](excel-matrix-2.png)

<i class="fa fa-exclamation-triangle"></i> The curly brackets `{ }` appear automatically by pressing `<Ctrl> + <Shift> + <Enter>` instead of just `<Enter>` and indicate that the formula is an array formula <a href="https://support.office.com/fr-fr/article/instructions-et-exemples-de-formules-matricielles-7d94a64e-3ff3-4686-9372-ecfd5caa57c7?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-alt"></i></a>.

## Indexing

The `INDEX()` function allows us to access any element of a vector or matrix, calling the function using `MonVector` as its first parameter returns the third element of our `A1:A4` vector. The function call `INDEX(MaMatrice, 3, 2)`returns the element located on row `3`, column `2` of our `A1:B4` matrix.

![`A6=INDEX(MonVecteur, 3)`](excel-index.png)

