---
title: Graphiques
keywords: graphique boursier, graphique manuel, ajouter traces, graphique excel
description: Les graphiques recommandés d'Excel et les outils de graphique.
weight: -2
---

## Graphiques recommandés

1. Insérer un nouveau [graphique](https://support.office.com/fr-fr/article/types-de-graphique-disponibles-dans-office-a6187218-807e-4103-9e0a-27cdb19afb90?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA#OfficeVersion=Windows) : `CTRL + A > Insérer > Graphique boursier`

![`CTRL + A > Insérer > Graphique boursier`](1_ohlcv.png)

2. Suivre les instructions s'il y a lieu :

![`Attention!`](2_ohlcv_error.png)

3. Ajouter le titre : `XSP.TO`

![`XSP.TO`](3_ohlcv_chart.png)

## Outils de graphique

1. Insérer un nouveau graphique : `Insérer > Nuage de points`

![`Insérer > Nuage de points`](1_xlsx_insert_chart.png)

2. Cliquer sur le graphique vide.

![`Clic`](2_xlsx_double_click.png)

3. Ajouter les données.

![`Selection de données`](3_xlsx_select_data_port.png)

4. Choisir un style de [graphique](https://support.office.com/fr-fr/article/modifier-la-disposition-ou-le-style-d-un-graphique-a346e438-d22a-4540-aa87-bce9feb719cf?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA).

![`Styles prédefinis`](4_xlsx_layout.png)

5. Exemple de graphique.

![Frontière](5_xlsx_chart.png)

