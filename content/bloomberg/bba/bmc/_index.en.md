---
title: Certification
keywords: bloomberg certificate, certification, bmc, bloomberg market concepts
description: Completing the Bloomberg certification using the terminal.
weight: 0
---

{{%notice tip%}}
It's possible to complete the *Bloomberg Market Concepts* (BMC) certification using a web browser by following [this instructions](../web/#bmc-login).
{{%/notice%}}

If you'd rather use the terminal to complete the Bloomberg certification, follow the instructions below.

## Using the terminal

{{%tabs%}}

{{%tab "Instructions"%}}

1. Type `BMC` on the Bloomberg command line and then hit `<Enter>`.

2. Click on `Sign up` and fill out the form using your institutional e-mail, e.g. `@ulaval.ca`.

> If a class code has been assigned for your class, enter it. For example, students at Université Laval may use code `JP9Z62R7KB` unless otherwise instructed.

3. Click on `Login`.

4. Click on `Access BMC for FREE`.

{{%/tab%}}

{{%tab "Terminal"%}}

1. `BMC <Enter>`

![](bmc.png)

2. `<6> <Enter>`

![](bmc_1.png)


> If a class code has been assigned for your class, enter it. For example, students at Université Laval may use code `JP9Z62R7KB` unless otherwise instructed.

![](bmc_2.png)

3. `<7> <Enter>`

![](bmc_3.png)

4. `Access BMC for FREE`

![](bmc_4.png)

{{%/tab%}}

{{%/tabs%}}

