---
title: Certification
keywords: certificat bloomberg, certificat bmc, comment, certification bloomberg, bmc
description: Comment faire la certification Bloomberg sur le terminal.
weight: 0
---

{{%notice tip%}}
Il est possible de compléter la certification *Bloomberg Market Concepts* (BMC) avec un navigateur web en suivant [ces instructions](../web/#compte-bmc).
{{%/notice%}}

Si vous préférez faire la certification Bloomberg sur le terminal au lieu de la version web, suivez les instructions ci-bas.


## Avec le terminal

{{%tabs%}}

{{%tab "Instructions"%}}

1. Tapez `BMC` sur la ligne de commande Bloomberg. Ensuite, appuyez sur la touche `<Entrée>`.
2. Cliquez sur `Sign up` et complétez le formulaire avec votre courriel institutionnel, par exemple `@ulaval.ca`.

> Si un code vous a été assigné dans le cadre d'un cours, rentrez-le. Par exemple, les étudiants de l'Université Laval peuvent utiliser le code `JP9Z62R7KB` sauf indication contraire.

3. Cliquez sur `Login`.
4. Cliquez sur `Access BMC for FREE`.

{{%/tab%}}

{{%tab "Terminal"%}}

1. `BMC <Entrée>`

![](bmc.png)

2. `<6> <Entrée>`

![](bmc_1.png)

> Si un code vous a été assigné dans le cadre d'un cours, rentrez-le. Par exemple, les étudiants de l'Université Laval peuvent utiliser le code `JP9Z62R7KB` sauf indication contraire.

![](bmc_2.png)

3. `<7> <Entrée>`

![](bmc_3.png)

4. `Access BMC for FREE`

![](bmc_4.png)

{{%/tab%}}

{{%/tabs%}}
