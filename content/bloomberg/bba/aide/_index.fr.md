---
title: Touche Fin
keywords: touche retour, fin, retour, clavier, bloomberg, clavier bloomberg, touche fin, end
description: Comment personnaliser le comportement de la touche Fin/End.
weight: 0
---

## Comportement par défaut

Le comportement par défaut de la touche `<Fin>` vous ramène à la page précédent et ensuite à la page d'accueil du terminal. Si vous préférez activer la navigation à l'aide de l'historique des fonctions, suivez ces instructions :

{{%tabs%}}
{{%tab "Instructions"%}}

1. Accédez aux paramètres par défaut en rentrant `PDFU 10` dans la ligne de commande suivi de la touche `<Entrée>`.

2. Cliquez sur `On` pour activer la navigation à l'aide de l'historique des fonctions utilisées.

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-pdfu.gif)

{{%/tab%}}


{{%tab "?"%}}

{{%notice tip%}}
Vous pouvez apprendre davantage sur le fonctionnement du clavier dans l'aide Bloomberg. Tapez `LPHP STOP:0:1 2614234` suivi de la touche `<Entrée>`. Un guide de démarrage est disponible dans la fonction `UNI` sous la section `Resources`.
{{%/notice%}}

![](clavier-help.gif)

{{%/tab%}}

{{%/tabs%}}


