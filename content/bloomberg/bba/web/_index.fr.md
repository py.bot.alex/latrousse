---
title: "Accès web"
date: 2020
icon: "ti-pulse"
description: Comment se connectrer à Bloomberg avec un navigateur web.
keywords: bloomberg, connexion, bmc, certificat, drs, bba, anywhere
---

Vous devez d'abord suivre les instructions pour la création d'un compte `BMC` (certification Bloomberg) et ensuite faire une demande pour la création d'un nom d'utilisateur pour le terminal Bloomberg à partir du portail `BMC`. Vous aurez donc deux comptes qui doivent être liés à votre adresse de courriel institutionnelle, par exemple `@ulaval.ca`.

{{%notice note%}}
Il est possible *temporairement* de se connecter à un terminal Bloomberg à l'aide d'un navigateur web en suivant les instructions ci-bas.
{{%/notice%}}

## Compte BMC

1. Allez à l'adresse `https://portal.bloombergforeducation.com/register` et créez un nouveau compte avec votre adresse de **courriel institutionnel**, par exemple `@ulaval.ca`.

![](register.gif)

{{%notice info%}}
Si une carte de crédit vous est demandée, veuillez plutôt contacter Bloomberg par courriel à l'adresse `bbg.edu@bloomberg.net` pour éviter des frais.
{{%/notice%}}

2. Vérifiez votre **courriel institutionnel**, par exemple `@ulaval.ca`, et cliquez sur le lien envoyé pour compléter l'inscription.

3. Une fois votre compte `BMC` activé, vous pouvez vos connecter à l'adresse `https://portal.bloombergforeducation.com` .

![](login.gif)


## Compte Terminal

{{%notice info%}}
Avant de pouvoir créer un compte pour le terminal vous devez contacter la personne en charge de la gestion des licences Bloomberg dans votre Université. Les étudiants de l'Université Laval doivent écrire à l'adresse `assistance.sdm@fsa.ulaval.ca`.
{{%/notice%}}

1. Connectez-vous sur le portail `BMC` {{%extlink "https://portal.bloombergforeducation.com/"%}} avec votre **courriel institutionnel**, par exemple `@ulaval.ca` .

![](login.gif)

2. Cliquez sur l'onglet `Terminal Access` pour faire une demande de création de compte pour le terminal Bloomberg.

![](terminal_access.gif)

{{%notice info%}}
Si vous avez utilisé votre **courriel institutionnel**, par exemple `@ulaval.ca`, mais vous ne voyez pas l'onglet `Terminal Access`, envoyez-nous un courriel à l'adresse `assistance.sdm@fsa.ulaval.ca`.
{{%/notice%}}

3. Attendez l'appel/message de Bloomberg. Vous allez ensuite recevoir un nom d'utilisateur et mot de passe à utiliser pour vous connecter au **terminal Bloomberg**.

## Connexion au terminal

{{%notice info%}}
Il est **interdit** de déconnecter un autre étudiant **sauf s'il s'agit de votre plage réservée**. Les étudiants de l'Université Laval doivent réserver une plage horaire sur le site de la FSA ULaval {{%extlink "http://applications.fsa.ulaval.ca/salle3324/"%}}.
{{%/notice%}}

![](dontkick.png)

1. Allez à l'adresse `https://bba.bloomberg.net` et connectez-vous avec votre nom d'utilisateur pour le `Terminal`.

{{%notice note%}}
Vous devez utiliser votre nom d'utilisateur du `Terminal` et **non** celui du portail BMC.
{{%/notice%}}

![](bba_login.gif)

2. Cliquez sur la flèche à la droite du bouton `LAUNCH` et ensuite sur `Launch within the browser`. Vous êtes maintenant connecté  à **Bloomberg** via votre navigateur web.

{{%notice tip%}}
Il est recommandé d'installer [Citrix](https://bba.bloomberg.net/Help?section=requirements) pour utiliser Bloomberg à Distance au lieu de la version web. Il suffit de cliquer sur `LAUNCH` au lieu de la flèche à la droite du bouton. Citrix est disponible pour Windows, MAC et Linux {{%extlink "https://bba.bloomberg.net/Help?section=requirements"%}}.
{{%/notice%}}
