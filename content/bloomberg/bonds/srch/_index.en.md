---
title: Search
keywords: fixed income search, bonds, fixed income, findig bonds
description: "[Bonds] Finding corporate/governement bonds with Bloomberg"
weight: 0
---

The `SRCH` function (*Fixed Income Search*) is used to find bonds using custom filters such as the market where the bonds are traded, the coupon paid, bond duration, etc.


## SRCH Function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter `SRCH` on the Bloomberg command line and press the `<Enter>` key to access the `Fixed Income Search` function.

2. Choose the `Country of Risk`, `Maturity type`, `Pricing Date` or any other criteria available for filtering.

{{%notice tip%}}
The *Example Searches* tab offers multiple sample queries.
{{%/notice%}}

3. Click on `Results` to obtain a list of bonds according to the filters used.

4. Click on any bond to get its **Bloomberg ticker** and relative functions.

> You can sort the results by clicking on any column name. For instance, clicking on the `Maturity Type` column will sort the data accordingly.

{{%notice tip%}}
If you'd like to check the start date of a security's price history, enter `GP` and press the `<Enter>` key, then click on `Max`.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](srch.gif)

{{%/tab%}}
{{%/tabs%}}

## RELS Function

The `RELS` function shows the securities related to a given company. You can use this function if you are looking for bonds issued by a given company.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter the name of the company on the Bloomberg command line and choose its ticker from the list.

2. Make sure the name of the company is shown on the top left corner of the screen : this means that the security is loaded on the terminal.

3. Enter `RELS` and hit `<Enter>` to access the home page of the `Related Securities` function.

4. From the `RELS` home page, on the `Debt` section, click on `Bonds & Generic Tickers`.

5. Use the arrows on the first line of the results table to filter/sort the data.

6. Click on a bond to load it on the Bloomberg terminal, you can and get its **ticker** using the `DES` function.

{{%/tab%}}
{{%tab "Terminal"%}}

![](rels.gif)

{{%/tab%}}
{{%/tabs%}}

