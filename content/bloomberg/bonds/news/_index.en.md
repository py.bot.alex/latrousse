---
title: News
keywords: news on a company, company news, bond news, fixed income, finding news
description: Finding company news with Bloomberg
weight: 0
---

The `CN` function helps us in finding news concerning a given company.

{{%notice note%}}
By default, all news are filtered depending on the loaded ticker shown on the top left corner of the Bloomberg screen.
{{%/notice%}}

## CN Function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter a company/bond ticker and hit `<Enter>`.

2. From the Bloomberg command line, type `CN` and hit `<Entrée>`.

3. Apply some filters to the results, i.e. you can choose the start and end dates of your research.

4. To save a news story, click on `Actions` and copy the web access link (if available) or choose `download`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](cn.gif)

{{%/tab%}}
{{%/tabs%}}

