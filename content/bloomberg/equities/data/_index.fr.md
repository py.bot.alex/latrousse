---
title: Rendements historiques
keywords: rendements historiques, extraction excel, api, bloomberg api
description: "Comment obtenir des rendements historiques avec Bloomberg"
weight: 0
---

Il est possible d'obtenir le rendement d'un titre (incluant les dividendes versés) grâce à l'API Bloomberg sur Excel. Pour ce faire, il suffit de choisir le titre désiré et d'extraire le champ `DAY_TO_DAY_TOT_RETURN_GROSS_DVDS`.

{{%notice tip%}}
Utilisez la fonction `FLDS` sur le terminal pour trouver d'autres champs d'extraction disponibles. Par exemple, le champ `DAY_TO_DAY_TOT_RETURN_NET_DVDS` correspond au champ *RT113 - Day to Day Total Return (Net Dividends)* et les détails du calcul sont disponibles via `FLDS`.
{{%/notice%}}

## API via Excel

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur l'onglet `Bloomberg` et ensuite sur `Spreadsheet Builder`.
2. Dans la sous-section `Analyze Historical Data` : cliquez sur `Historical Data Table` et ensuite sur `Next`.
3. Sélectionnez le ou les titres dont vous désirez extraire les données.
4. Sélectionnez les champs souhaités.
5. Choisissez les dates de début et de fin de l'historique ainsi que la fréquence des données.
6. Sélectionnez la cellule Excel où se trouveront les données et cliquez sur `Finish`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](excel.gif)

{{%/tabs%}}
{{%/tabs%}}

