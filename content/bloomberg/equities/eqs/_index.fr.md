---
title: Trouver une action
keywords: recherche d'actions, actions, screening, trouver actions, eqs
description: Comment appliquer des filtres pour des actions avec Bloomberg.
weight: 0
---

La fonction `EQS` vous permet de trouver des titres qui satisfont les critères désirés (marchés, secteur, ratios, date d'introduction en bourse, etc.).

## Fonction EQS

{{%tabs%}}
{{%tab "Instructions"%}}

1. Depuis la ligne de commande Bloomberg, tapez `EQS` et appuyez sur la touche `<Entrée>`.
2. Choisissez vos critères de filtrage dans la page d'accueil de la fonction `EQS`.

> À cette étape, vous pouvez ajouter des critères personnalisés et utiliser des opérateurs logiques.

3. Une fois les filtres nécessaires appliqués, appuyez sur `1) Results` pour obtenir la liste des titres correspondant à vos critères de recherche.
4. Exportez le résultat obtenu vers un fichier Excel au besoin.

{{%/tab%}}
{{%tab "Terminal"%}}

![](eqs.gif)

{{%/tab%}}
{{%/tabs%}}

