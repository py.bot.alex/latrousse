---
title: Monitoring
keywords: derivatives, options, monitor, chain, call, put, gsf-3101, omon
description: Real time  monitoring of options data with Bloomberg.
weight: 0
---

The `OMON` function allows real time monitoring of options data through a customizable interface.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Type a Bloomberg ticker on the command line, e.g. `POW CN Equity`, and hit `<Enter>` to load it on the terminal.

2. Type `OMON` and hit `<Enter>` to launch the `Option Monitor` function.

3. Choose the desired parameters on the `Control Area` top panel.

{{%/tab%}}
{{%tab "Help"%}}

+ Center the option chain using the `Center` price field.
+ Control the number of strike prices displayed using the `Strikes` parameter.
+ Set the `Expiry` using the date amber field next to `Exp`.
+ See up to 120 days of historical option chain data by setting the `As of` date field.

{{%notice tip%}}
You can learn every detail about the `OMON` function by accessing its official Bloomberg help page. Type `HELP OMON` and hit `<Enter>` to learn more!
{{%/notice%}}

{{%/tab%}}


{{%tab "Terminal"%}}

![](omon_use.gif)

{{%/tab%}}

{{%/tabs%}}
