---
title: Search
keywords: derivatives, options, search, call, put, strangle, straddle, gsf-3101, osch
description: Finding options with Bloomberg.
weight: 0
---

The `OSCH` function lets you apply filters to narrow your option search. You can also add `Strategy` criteria to your screening and additional display fields.

## `OSCH` function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Type `OSCH` on the Bloomberg command line and hit `<Enter>` to access the `Option Search` function.

2. Click on `95) Actions` and then on `7) Clear Search`.

3. Select the `Sectors`, `Country of Domicile`, `Strategy` or any other criteria and click on `1) Results`.

{{%notice tip%}}
You can add additional columns to your results by using the `Add a Display Field` search box on top of the results table.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

1. Type `OSCH` on the Bloomberg command line and hit `<Enter>`.

2. Clear Search : Type `95` and hit `<Enter>`, then `7 <Enter>`

3. Stocks only : `31 <Enter>` followed by `<Alt> ETF <Enter> 1 <Enter>`.

4. Sectors : `32 <Enter>`, drag and drop `Financials`, `1 <Enter>`.

5. Strategy criteria (Strangle) : `56 <Enter> 5 <Enter> 1 <Enter>`.

6. Country of Domicile : `34 <Enter>`, drag and drop `Canada`, `1 <Enter>`.

7. View `1) Results` : `1 <Enter>`.

![](osch_use.gif)

{{%/tab%}}
{{%/tabs%}}

## Historical Prices

{{%tabs%}}
{{%tab "?"%}}

+ You can obtain historical option prices on the Bloomberg terminal using the `HP` function as explained [below](#osch-hp).
+ The `TMX Group` offers web access to **6 months** of historical options data on the `Montreal Exchange` website {{%extlink "https://www.m-x.ca/nego_fin_jour_fr.php"%}}.

{{%/tab%}}
{{%tab "TMX"%}}

![](hp_tmx.gif)

{{%/tab%}}
{{%/tabs%}}

## `HP` function {#osch-hp}


{{%tabs%}}
{{%tab "Instructions"%}}

In this example we start by using the `MOSO` function to pick one of the `Most Active Options`. We then use the `HP` function to export a table of `Bid Price` and `Implied Volatility Mid` to Excel.

### Picking an option from `MOSO`

1. Type `MOSO` on the Bloomberg command line and hit `<Enter>` to access the `Most Active Options` function.

2. Hit the `<Tab>` key and select `Single Security` from the top left amber dropdown menu.

3. Hit the `<Tab>` key twice and enter a Bloomberg ticker, e.g. `POW CN Equity`, then hit `<Enter>`.

4. Click on one of the `options` from the `Top Options` table. Its ticker should appear on the top left corner of the terminal.

### Historical prices with the `HP` function

1. Enter a Bloomerg option ticker, e.g. `POW CN 10/16/20 C25`, and hit `<Enter>` to load in on the terminal. The ticker should appear on the top left corner of the terminal.

2. Type `HP` on the Bloomberg command line and hit `<Enter>`.

3. Choose the desired fields (Dates, Frequency, Columns, etc.) for the data table, e.g. `Bid Price (PX_BID)` and `IV Mid (IVOL_MID)`.

4. Click on `95) Export` to create an Excel file with the data.

{{%/tab%}}
{{%tab "Terminal"%}}

### Picking a function from `MOSO`

1. Type `MOSO` on the Bloomberg command line and hit `<Enter>` to access the `Most Active Options` function.

2. Single Security : `<Tab> SINGLE SECURITY <Enter>`. Then `<Tab> <Tab> POW CN <F8> <Enter>`.

4. Choose among most active options : `<Alt> 16OCT20C25 <Enter>`. The ticker should appear on the top left corner of the terminal.

### Launching the `HP` function

1. Enter a Bloomerg option ticker, e.g. `POW CN 10/16/20 C25`, and hit `<Enter>` to load in on the terminal. The ticker should appear on the top left corner of the terminal.

2. HP Table : `HP <Enter>`.

3. Table parameters : choose the desired dates, frequency, columns, etc. for the data table. In the example we are using the `Bid Price (PX_BID)` and `IV Mid (IVOL_MID)` fields.

4. Click on `95) Export` to create an Excel file with the data.

![](hp_options.gif)

{{%/tab%}}
{{%/tabs%}}

