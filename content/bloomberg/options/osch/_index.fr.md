---
title: Recherche
keywords: produits, dérivés, recherche, de, options, achat, vente, strangle, straddle, gsf-3101, osch
description: Comment trouver des options avec Bloomberg.
weight: 0
---

La fonction `OSCH` vous permet d'appliquer des filtres à votre recherche d'options. Vous pouvez même utiliser une `Stratégie` comme critère ainsi que des champs d'affichage additionnels.

## Fonction `OSCH`

{{%tabs%}}
{{%tab "Instructions"%}}

1. Tapez `OSCH` sur la ligne de commande Bloomberg et appuyez sur la touche `<Entrée>` pour accéder à la fonction `Option Search`.

2. Cliquez sur `95) Actions` et ensuite sur `7) Clear Search`.

3. Choisissez les `Secteur`, `Pays de Domicile`, `Stratégie` ou tout autre critère de recherche et cliquez sur `1) Results`.

{{%notice tip%}}
Vous pouvez ajouter des colonnes additionnelles aux résultats à l'aide de la barre de recherche `Add a Display Field` en haut du tableau de résultats.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

1. Tapez `OSCH` sur la ligne de commande Bloomberg et appuyez sur `<Entrée>`.

2. Nouvelle recherche : Tapez `95` et appuyez sur `<Entrée>`, ensuite faites la combinaison `7 <Entrée>`

3. Actif sous-jacent (actions) : `31 <Entrée>` et ensuite `<Alt> ETF <Entrée> 1 <Entrée>`.

4. Secteur : `32 <Entrée>`, glissez-déposez `Financials`, `1 <Entrée>`.

5. Stratégie (Strangle) : `56 <Entrée> 5 <Entrée> 1 <Entrée>`.

6. Pays de domicile : `34 <Entrée>`, glissez-déposez `Canada`, `1 <Entrée>`.

7. Voir les `1) Results` : `1 <Entrée>`.

![](osch_use.gif)

{{%/tab%}}
{{%/tabs%}}

## Prix historiques

{{%tabs%}}
{{%tab "?"%}}

+ Vous pouvez obtenir des données historiques sur les options à l'aide de la fonction `HP` en suivant les instructions [ci-bas](#osch-hp).
+ Le `Group TMX` offre jusqu'à **6 mois** de données historiques sur le site web de la `Bourse de Montréal` {{%extlink "https://www.m-x.ca/nego_fin_jour_fr.php"%}}.

{{%/tab%}}
{{%tab "TMX"%}}

![](hp_tmx.gif)

{{%/tab%}}
{{%/tabs%}}

## Fonction `HP` {#osch-hp}


{{%tabs%}}
{{%tab "Instructions"%}}

Nous commençons par choisir une action activement transigée à l'aide de la fonction `MOSO`. Ensuite, on se sert de la fonction `HP` pour exporter vers Excel un tableau avec le `Prix Bid` et la `Volatilité Implicite Mid`.

### Choisir une option avec `MOSO`

1. Tapez `MOSO` sur la ligne de commande Bloomberg et appuyez sur `<Entrée>` pour accéder à la fonction `Most Active Options`.

2. Appuyez sur la touche `<Tab>` et choisissez `Single Security` dans le menu déroulant du coin supérieur gauche de l'écran.

3. Appuyez deux fois sur la touche `<Tab>` et entrez le ticker de l'actif sous-jacent, par exemple `POW CN Equity`, ensuite appuyez sur `<Entrée>`.

4. Cliquez sur une `option` du tableau `Top Options`. Assurez-vous de voir le ticker dans le coin supérieur gauche du terminal.

### Prix historiques avec la fonction `HP`.

1. Rentrez le ticker Bloomberg d'une option, par exemple `POW CN 10/16/20 C25`, et appuyez sur la touche `<Entrée>`. Assurez-vous de voir le ticker dans le coin supérieur gauche du terminal.

2. Tapez `HP` sur la ligne de commande et appuyez sur `<Entrée>`.

3. Choisissez les champs à afficher sur le tableau (Dates, Fréquence, Colonnes, etc.), par exemple `Bid Price (PX_BID)` et `IV Mid (IVOL_MID)`.

4. Cliquez sur `95) Export` pour créer un fichier Excel avec ces données.

{{%/tab%}}
{{%tab "Terminal"%}}

### Choisir une option avec `MOSO`

1. Tapez `MOSO` sur la ligne de commande Bloomberg et appuyez sur `<Entrée>`.

2. Single Security : `<Tab> SINGLE SECURITY <Entrée>`. Après `<Tab> <Tab> POW CN <F8> <Entrée>`.

4. Choisir parmi les options les plus actives : `<Alt> 16OCT20C25 <Entrée>`.

### Lancer la fonction `HP`

1. Rentrez le ticker Bloomberg d'une option, e.g. `POW CN 10/16/20 C25`, et appuyez sur `<Entrée>`.
2. Tableau HP : `HP <Entrée>`.

3. Paramètres du tableau : Choisissez les dates, fréquence, colonnes, etc., par exemple `Bid Price (PX_BID)` et `IV Mid (IVOL_MID)`.

4. Cliquez sur `95) Export` pour créer un fichier Excel avec ces données.

![](hp_options.gif)

{{%/tab%}}
{{%/tabs%}}

