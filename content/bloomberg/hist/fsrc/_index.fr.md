---
title: Trouver un fonds
keywords: fonds, screening, placement, fermés, ouverts, mutuel, prix, excel, télécharger, fsrc
description: La fonction FSRC nous permet d'appliquer divers filtres pour trouver des fonds.
weight: 0
---

La fonction `FSRC` nous permet d'appliquer divers filtres pour trouver des fonds. Nous pouvons par la suite utiliser les tickers (identifiants uniques) pour obtenir des données historiques.

## Fonction `FSRC`

{{%tabs%}}
{{%tab "Instructions"%}}

1. Tapez `fund screen` sur la ligne de commande et cliquez sur `FSRC`.

2. Cliquez sur `Fund type` et choisissez `Closed end`.

3. Cliquez sur `Country of domicile` et ensuite sur `Canada`.

4. Cliquez sur `Results`.

{{%notice tip%}}
Pour connaître tous les détails sur les filtres disponibles, appuyez sur la touche `<F1>` à partir de la fonction `FSRC`.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-fsrc.gif)

{{%/tabs%}}
{{%/tabs%}}

## Données via `Excel`

{{%tabs%}}
{{%tab "Instructions"%}}

1. À partir de la page des résultats de la fonction `FSRC`, cliquez sur `Export`.

2. À partir du fichier Excel que vous venez d'exporter, cliquez sur `Bloomberg` et ensuite sur `Spreadsheet Builder`.

3. Cliquez sur `Historical Data Table` et ensuite sur `Next`.

4. Sélectionnez la plage `Excel` où se trouvent les tickers (identifiants uniques) et cliquez sur `Next`.

5. Cherchez par mot clé, par exemple `Net Asset Value`. Choisissez le champ `FUND_NET_ASSET_VAL` et cliquez sur `Add`, ensuite sur `Next`.

6. Ajustez les paramètres pour l'étendue et la fréquence des données, ensuite cliquez sur `Next`.

7. Sélectionnez la plage `Excel` où se trouveront les données, ensuite cliquez sur `Finish`.

{{%notice tip%}}
Sur la **version web** de Bloomberg, il faut suivre [ces instructions](http://sdmapps.ca/latrousse/fr/bloomberg/bba/excel-citrix/) pour télécharger les données sur son ordinateur.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-fsrc-excel.gif)

{{%/tabs%}}
{{%/tabs%}}
