---
title: Prix
keywords: données historiques, prix historiques, tableau hp, exporter prix, données bloomberg, obtenir données, bloomberg api, api
description: Comment obtenir des prix historiques avec Bloomberg.
weight: 0
---

La fonction Bloomberg `HP` permet d'accéder à des données historiques sur le terminal. Il est aussi possible d'exporter ces données vers Excel en suivant les instructions ci-bas.

## Fonction `HP`

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le ticker Bloomberg du titre recherché sur la ligne de commande, par exemple `TSLA US Equity`, et appuyez sur la touche `Entrée`.

{{%notice tip%}}
Au moment de tapez le mot `Equity`, appuyez sur la touche `<F8>` au lieu de rentrer lettre par lettre le mot `EQUITY`.
{{%/notice%}}

2. Tapez `HP` sur la ligne de commande Bloomberg et appuyez sur la touche `Entrée` pour lancer la fonction `Historical Price Table`.

3. Choisissez les paramètres désirés pour le tableau de données.

{{%/tab%}}
{{%tab "Terminal"%}}

![](hp_use.gif)

{{%/tabs%}}
{{%/tabs%}}

## Exporter le tableau `HP`

{{%tabs%}}

{{%tab "Instructions"%}}

### Terminal Bloomberg
1. À partir de la fonction `HP`, cliquez sur `96) Export`. Si vous préférez, il est aussi possible de taper le nombre à gauche de `96) Export` sur la ligne de commande suivi de la touche `Entrée`, c'est-à-dire `96 <Entrée>`.
2. À partir du fichier Excel que vous venez d'exporter, cliquez sur `Fichier` et ensuite sur `Enregistrer sous`[^1] pour enregistrer une copie des données.
[^1]: Vous pouvez aussi appuyer sur la touche `<F12>` pour ouvrir la fenêtre `Enregistrer sous`.

{{%notice note%}}
Puisque vous utilisez un terminal Bloomberg, ceci ouvrira un fichier Excel avec les données sur **votre ordinateur**. Si vous accédez à Bloomberg via **Citrix**, incluant la version web, vous devez suivre [ces instructions](../../bba/excel-citrix/#version-web) pour enregistrer une copie du fichier sur votre ordinateur.
{{%/notice%}}

{{%/tab%}}

{{%tab "Terminal"%}}
### Terminal Bloomberg
![](hp_export_win.gif)
### Bloomberg Anywhere
![](hp_export.gif)
{{%/tab%}}

{{%/tabs%}}
